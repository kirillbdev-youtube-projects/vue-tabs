export default {
  data: function () {
    return {
      tabs: []
    };
  },
  created: function () {
    this.tabs = this.$children;
  },
  methods: {
    setActive: function (tab) {
      this.tabs.forEach(function (t) {
        t.active = t === tab;
      });
    }
  },
  template: `
    <div class="tabs">
      <ul class="tab-links">
        <li v-for="tab in tabs" :class="{ 'active': tab.active }" @click="setActive(tab)">{{ tab.label }}</li>
      </ul>
      <slot></slot>
    </div>
  `
};