export default {
  props: [ 'active', 'label' ],
  data: function () {
    return {
      active: this.active || false
    };
  },
  template: `
    <div class="tab-pane" :class="{ 'active': active }">
      <slot></slot>
    </div>
  `
};