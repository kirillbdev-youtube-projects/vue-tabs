import Vue from 'vue/dist/vue.esm.js';
import Tabs from './components/tabs';
import Tab from './components/tab';

Vue.component('tabs', Tabs);
Vue.component('tab', Tab);

let app = new Vue({
  el: '#vue-tabs-app'
});