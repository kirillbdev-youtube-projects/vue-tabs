const gulp = require('gulp'),
      rename = require('gulp-rename'),
      sass = require('gulp-sass'),
      cleanCss = require('gulp-clean-css');

gulp.task('build-scss', function () {
  return gulp.src('./src/scss/main.scss')
    .pipe(sass())
    .pipe(cleanCss())
    .pipe(rename('style.min.css'))
    .pipe(gulp.dest('./assets/css'));
});

gulp.task('watch-scss', function () {
  return gulp.watch('src/scss/**/*.scss', gulp.series('build-scss'));
});